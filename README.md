Shaders Live Wallpaper is the LibGDX powered Android live wallpaper
based on GLSL fragment shaders from glslsandbox.com or shadertoy.com.

How to add new shader?

1. Create file with shader source code in android/assets folder

2. Create shader icons and place it to appropriate android/res/drawable folders

3. Add information about shader to shaders array in android/src/com/fasthamster/shaderslw/ShadersHandler.java file